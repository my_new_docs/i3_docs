# i3_docs
Without a display/login manager.

 

-To login automatically (at tty1) after boot do sudo systemctl edit getty@tty1 and paste this:

 

[Service]  
ExecStart=  
ExecStart=-/sbin/agetty --autologin YourUserName --noclear %I 38400 linux  

 

ref. https://wiki.archlinux.org/index.php/Automatic_login_to_virtual_console#Virtual_console

 

-To automatically start X after tty login add the following to the bottom of ~/.bash_profile (if the file does not exist, copy a skeleton version from /etc/skel/.bash_profile):

 

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx  

 

ref. https://wiki.archlinux.org/index.php/Start_X_at_Boot#Shell_profile_files  

 

-To execute i3lock after i3 session is executed simply add i3lock to your ~/.i3/config file:

exec --no-startup-id bash -c 'i3lock'

#FEH 
exec --no-startup-id feh --randomize --bg-fill ~/Pictures/Wallpapers/*

#Bumbleebee-status
sudo apt-get install python-netifaces python-psutil fonts-powerline fonts-font-awesome 

sudo cat ~/.config/i3/config
font pango:FontAwesome 9 
status_command /opt/bumblebee-status/bumblebee-status -m cpu memory battery date time pasink pasource dnf -p time.format="%H:%M CW %V" date.format="%a, %b %d %Y" -t solarized

#neofetch 
sudo apt install neofetch imagemagick w3m-img
neofetch --w3m --path_image
